import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { navigate } from "@reach/router";
import * as config from '../../system/config';

import { setLoginStatus } from './action_action';

function Login() {
    const [user, setLogin] = useState({ email: '', password: '' });

    const dispatch = useDispatch();
    useEffect(() => {
        if (localStorage.removeItem('token') == null) {
            navigate("login");
        }
    },[]);
    const handleLoginSubmit = (e) => {
        e.preventDefault();
        const data = { email: user.email, password: user.password };

        axios.post(config.API_URL + config.API_LOGIN, data)
            .then((responce) => {
                if (responce.data.success == 0) {
                    alert(responce.data.message);

                } else {
                    alert(responce.data.message);
                    localStorage.setItem('token', responce.data.token);
                    dispatch(setLoginStatus(true));
                    navigate("dashboard");

                }
            })
            .catch(function (error) {
                console.log("error in login page : ", error);
            });

    };

    const onChange = (e) => {
        e.persist();
        setLogin({ ...user, [e.target.name]: e.target.value });
    }
    return (

        <div className="container mt-5 mb-5 ">
            <div className="row">

                <div className="col-lg-3"></div>
                <div className="col-lg-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="text-center">
                                <h1 className="h4  mb-4">Login</h1>
                            </div>
                            <div className="p-2">
                                <form onSubmit={handleLoginSubmit} className="user">
                                    <div className="form-group mb-2">
                                        <input type="email" className="form-control" value={user.Email} onChange={onChange} name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email" />
                                    </div>
                                    <div className="form-group  mb-2">
                                        <input type="password" className="form-control" value={user.Password} onChange={onChange} name="password" id="password" placeholder="Password" />
                                    </div>
                                    <hr />
                                    <button type="submit" className="btn btn-primary mb-1"><span>Login</span></button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-lg-3"></div>
        </div>

    )
}

export default Login