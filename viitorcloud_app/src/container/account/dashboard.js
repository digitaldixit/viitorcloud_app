import React, { useEffect } from 'react';
import * as api_config from '../../../src/system/config';
import axios from 'axios';
import { navigate } from "@reach/router";
import {useDispatch,useSelector} from 'react-redux';
import { setUserList } from '../account/action_action';
export default function Dashboard() {
  // const [users, setUserList] = React.useState([]);
  const [searchText, setSearchText] = React.useState([]);
  const dispatch =useDispatch();
  const users = useSelector(state=>state.account.user_list);
  // get user 
  const getUserList = async (searchText, sort) => {
    setSearchText("");
    var config = {
      method: "post",
      url: api_config.API_URL + api_config.API_GET_USERS,
      data: {
        search: searchText ? searchText : "",
        sort: sort ? sort : "",
      },
    };

    await axios(config)
      .then(function (response) {
        console.log("----response.data.result", response.data.result)
        if (response.data) {
          dispatch(setUserList(response.data.result))
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  // delete user 
  const deleteUser = (user_id) => {
    setSearchText("");
    var config = {
      method: "delete",
      url: api_config.API_URL + api_config.API_DELETE_USER + '/' + user_id,
    };

    axios(config)
      .then(function (response) {
        console.log("----delete record", response.data)
        if (response.data) {
          getUserList();
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  // handle code 
  const handleSearch = (e) => {
    e.preventDefault();
    getUserList(searchText, 'asc')
  };
  const applySortBy = (e) => {
    getUserList(searchText, e.target.value)
  }
  const handleDelete = (e, id) => {
    console.log("----id-", id)
    e.preventDefault();
    deleteUser(id);
  };
  const habdleViewInfo = (user) => {
    navigate("userinfo", {
      state: {
        userInfo: user,
      },
    });
  }
  useEffect(() => {
    setSearchText("");
    getUserList();
  }, [])


  console.log("----users", users);
  return (
    <div className="container mt-5 mb-5">


      <div className="row">

        <div className="col-sm-2">

        </div>

        <div className="col-sm-8">
          <div className="card-header">
            <div className="row">


              <div className="col-sm-6">
                <select onChange={(e) => applySortBy(e)} className="form-select">
                  <option value="">-- Sort By --</option>
                  <option value="ASC">ASC</option>
                  <option value="DESC">DESC</option>
                </select>
              </div>
              <div className="col-sm-6">
                <form onSubmit={handleSearch} className="">
                  <div className="input-group">

                    <input type="text" className="form-control" value={searchText} onChange={(e) => setSearchText(e.target.value)} placeholder="Search by Name" />
                    <div className="input-group-append">
                      <button className="btn btn-outline-secondary" type="submit" >Submit</button>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>

          <table className="table border mb-0">
            <thead>
              <tr>
                <th className="text-right">ID</th>
                <th className="text-left">Name</th>
                <th className="text-right"> Email</th>
                <th className="text-right">Phone Number</th>
                <th className="text-right"> Action</th>
              </tr>
            </thead>
            <tbody>

              {
                users && users.map((user, index) => (
                  <tr key={'user_' + index} className="user-row">
                    <td className="text-right">
                      <b>{user.id}</b>
                    </td>
                    <td className="text-right">
                      <b>{user.firstName}</b>
                    </td>
                    <td className="text-right">
                      {user.email}
                    </td>
                    <td className="text-right">
                      {user.number}
                    </td>
                    <td className="text-right">
                      <button type="submit" className="btn btn-sm btn-danger mr-2" onClick={(e) => handleDelete(e, user.id)}>delete</button>
                      <button type="submit" className="btn btn-sm btn-primary ml-2" style={{ 'marginLeft': '.5rem' }} onClick={() => {
                        habdleViewInfo(user);
                      }}>view</button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
        <div className="col-sm-2">

        </div>
      </div>
    </div>



  );
}