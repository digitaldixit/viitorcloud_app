import React from 'react';
import { Link } from "@reach/router";
export default function UserInfo(props) {

    let userData = props.location.state.userInfo;
    return (
        <div className="container mt-3">
            <div className="row">
                <div className="col-sm-3">
                </div>
                <div className="col-sm-6">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="mt-2">
                                User Details
                                <div className="pull-right">
                                    <Link to="/dashboard" className="btn btn-outline-primary ml-2">back</Link>
                                </div>

                            </h4>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table mb-0">
                                    <tbody>

                                        <tr>
                                            <td className="first  font-weignt-bold text-primary  h6 border-top-0">Teacher Name</td>
                                            <td className="second border-top-0 col-6">:  {userData?.firstName + '' + userData?.lastName}</td>
                                        </tr>

                                        <tr>
                                            <td className="first  font-weignt-bold text-primary  h6">Contact Phone</td>
                                            <td className="second border-top-0 col-6">:   {userData?.number}</td>
                                        </tr>
                                        <tr>
                                            <td className="first  font-weignt-bold text-primary h6 ">Email</td>
                                            <td className="second ">: {userData?.email}</td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-3">

                </div>
            </div>

        </div>

    )
}