
export const  SET_LOGIN_STATUS = 'SET_LOGIN_STATUS';
export const SET_USER_LIST = "SET_USER_LIST";

export function setLoginStatus(status){
    return function(dispatch){
    
        dispatch({
            type : SET_LOGIN_STATUS,
            payload : status
        })
    }
  
}
export function setUserList(users){
    return function(dispatch){
    
        dispatch({
            type : SET_USER_LIST,
            payload : users
        })
    }
  
}
   
