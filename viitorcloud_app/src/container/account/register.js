import React, { useState } from 'react'
import axios from 'axios';
import { navigate } from "@reach/router";
import * as config from '../../system/config';
function AccountRegister() {

    const [data, setdata] = useState({ email: '', password: '', firstName: '', lastName: '', number: '' })

    const handleRegistrationSubmit = (e) => {
        e.preventDefault();
        const registerData = { email: data.email, password: data.password, firstName: data.firstName, lastName: data.lastName, number: data.number }

        axios.post(config.API_URL + config.API_REGISTER, registerData)
            .then((responce) => {
                console.log("----------login register",responce.data);
                if (responce.data.error){
                     alert("-------regusrer ni not done plz add valid data")       
                }else{
                    navigate("login");
                }
            })
            .catch(function (error) {
                console.log("error in register page ");
            });
    };
    const onChange = (e) => {
        e.persist();
        setdata({ ...data, [e.target.name]: e.target.value });
    }
    return (

        <div className="container mt-5 mb-5 ">
            <div className="row">

                <div className="col-lg-3"></div>
                <div className="col-lg-6">
                    <div className="card">
                        <div className="card-body">
                            <div className="text-center">
                                <h1 className="h4  mb-4">Register</h1>
                            </div>
                            <form onSubmit={handleRegistrationSubmit} className="user">
                                <div className="form-group row">
                                    <div className="col-sm-6 mb-3">
                                        <input type="text" name="firstName" onChange={onChange} value={data.firstname} className="form-control" id="firstname" placeholder="First Name" />
                                    </div>
                                    <div className="col-sm-6">
                                        <input type="text" name="lastName" onChange={onChange} value={data.lastname} className="form-control" id="lastname" placeholder="Last Name" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-sm-6  mb-3">
                                        <input type="email" name="email" onChange={onChange} value={data.email} className="form-control" id="email" placeholder="email" />
                                    </div>
                                    <div className="col-sm-6">
                                        <input type="password" name="password" onChange={onChange} value={data.password} className="form-control" id="password" placeholder="password" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-sm-12  mb-3">
                                        <input type="text" name="number" onChange={onChange} value={data.number} className="form-control" id="number" placeholder="Mobile NO" />
                                    </div>
                                </div>
                                <hr />
                                <button type="submit" className="btn btn-primary  btn-block">
                                    Register Now
                                </button>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
            <div className="col-lg-3"></div>
        </div>


    )
}

export default AccountRegister;