import {
    SET_LOGIN_STATUS,
    SET_USER_LIST
} from './action_action';

export default function AccountReducer(state = {}, action) {
    switch (action.type) {
        case SET_LOGIN_STATUS:
            return { is_login_status: action.payload }
        case SET_USER_LIST:
            return { user_list: action.payload }
        default:
            return state;
    }

}

