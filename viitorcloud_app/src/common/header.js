import React, { Component } from 'react';
import { navigate ,Link} from "@reach/router";
import { useDispatch , useSelector } from 'react-redux';
import { setLoginStatus } from '../container/account/action_action';
export default function Header() {
  const dispatch = useDispatch();
  const loginStatus = useSelector(state => state.account.is_login_status)
const logout= (e) =>{
  e.preventDefault();
  localStorage.removeItem('token');
  dispatch(setLoginStatus(false));
  navigate("login");
                  
}
console.log("----loginStatus",loginStatus);
    return (

      <header className="edu-header bg-light" id="header-main">

        <nav className="navbar navbar-expand-lg">
          <div className="container">
          <Link to="/" className="navbar-brand">
                <span className="logo-text">React App</span>
            </Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon fa fa-bars"></span>
            </button>
           
            <div className="collapse navbar-collapse justify-content-end" id="mobile-navigation">
           
              <ul className="navbar-nav ml-4">
              {
                  loginStatus
                    ?
                    <React.Fragment>
                    <li className="nav-item">
                      <Link className="nav-link" aria-current="page" to="/dashboard">Dashboard</Link>
                    </li>
                    <li className="nav-item">
                      <button className="nav-link btn btn-outline-primary" aria-current="page" onClick={logout}>Logout</button>
                    </li>
                    </React.Fragment>
                    :
                    <React.Fragment>
                    <li className="alc-nav-item ml-4"><Link to="/login" className="btn btn-outline-primary">Login</Link></li>
                    <li className="alc-nav-item ml-2 "><Link to="/register" className="btn btn-outline-primary ms-2">Register</Link></li>
                    </React.Fragment>
                }
                
              </ul>
            </div>
          </div>
        </nav>

      </header>

    );
  

}

