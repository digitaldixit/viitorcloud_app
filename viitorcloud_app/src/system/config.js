export const API_URL = 'http://localhost:3004';
export const APP_URL = 'http://localhost:3000';

// All Api Config Here.....//
export const API_REGISTER = '/register';
export const API_LOGIN = '/login';
export const API_GET_USERS = '/users';
export const API_DELETE_USER = '/deleteUser';
