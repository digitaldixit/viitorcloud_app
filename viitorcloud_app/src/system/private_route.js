import React from "react";
import Login from "../container/account/login";

const PrivateRoute = ({ as: Component, ...props }) => {
  let token = localStorage.getItem("token");
  return (
    <div>
      {token ? (
        <Component {...props} />
      ) : (
        <Login/>
      )}
    </div>
  );
};

export default PrivateRoute;
