import { Router } from "@reach/router";

import Dashboard from '../container/account/dashboard';
import UserInfo from '../container/account/user_info';

import Login from '../container/account/login';
import Register from '../container/account/register';
import PrivateRoute from "./private_route";
const  routes = (
    <Router>
       <PrivateRoute as={Dashboard} path={"/dashboard"}/>
       <PrivateRoute as={UserInfo} path={"/userinfo"}/>
      <Login path={"/login"}/>
      <Register path={"/register"}/>
       <Register path={"/register"}/>
    </Router>
)
export default routes;