const { createUser, showUser, updateUser,deleteUser,storeinPdfs, login, editPassword } = require('./user.controller');
const router = require('express').Router();

router.post("/register",createUser);
router.post("/login", login)
router.post("/users",showUser);
router.delete("/deleteUser/:id",deleteUser);


module.exports = router; 
