const pool = require("../../config/database");
const fs = require('fs')
module.exports = {
    create: async (data, callBack) => {

        try {
            await pool.query(`insert into registration(firstName, lastName, email, password, number) values(?,?,?,?,?)`, [
                data.firstName,
                data.lastName,
                data.email,
                data.password,
                data.number
            ],

                (error, results, fields) => {
                    if (error) {
                        return callBack(error);
                    }
                    return callBack(null, results)

                })

        } catch (error) {
            console.log(" Error while creating user " + error)
        }
    },

    show: async (data, callBack) => {
        console.log("----data", data);
        var sqlQuery = "select * from registration  ";
      
        if (data.search != '' && data.search != undefined) {
            sqlQuery += " WHERE firstName LIKE '%"+data.search+"%'";
        }
        if (data.sort && data.sort != undefined && data.sort == 'DESC') {
            sqlQuery += " ORDER BY  firstName DESC";
        } else {
            sqlQuery += " ORDER BY  firstName ASC";
        }
        console.log("----sqlQuery", sqlQuery);
        try {
            await pool.query(sqlQuery,
                (error, results, fields) => {

                    if (results != undefined && results.length) {
                        return callBack(null, results)
                    } else {
                        return callBack(null, []);
                    }
                })

        } catch (error) {
            console.log(" Error while showing error " + error)
        }
    },

    deletes: async (data, callBack, req) => {
        try {

            await pool.query("DELETE FROM registration WHERE id =?", data,
                (error, results, fields) => {
                    if (error) {
                        return callBack(error);
                    }
                    return callBack(null, results)

                })
        } catch (error) {
            console.log(" Error while deleting entry " + error)
        }

    },

    storeinPdf: async (data, callBack) => {
        try {
            await pool.query("select id,firstName,gender,email,number from registration",
                (error, results, fields) => {
                    if (error) {
                        return callBack(error);
                    }
                    return callBack(null, results)

                });

        } catch (error) {
            console.log(" Error while storing in PDF " + error)
        }
    },

    getUserByEmail: async (email, callBack) => {
        try {
            await pool.query(`select * from registration where email = ?`, [email],
                (error, results, fields) => {
                    if (error) {
                        callBack("Error occured");
                    }
                    return callBack(null, results[0]);
                }
            );
        } catch (error) {
            console.log(" Error whie getting user by email " + error)
        }
    },

    editPassword: async (data, email, oldPassword, newPassword, confirmPassword, callBack) => {

        try {
            await pool.query(`select password from registration where password=?`, [oldPassword],
                (error, results, fields) => {
                    if (error) {
                        return callBack(error);
                    }
                    return callBack(null, results[0])

                })
            pool.query(`UPDATE registration SET password = ? where email=?`, [data.password, data.email,],

                (error, fields) => {
                    if (error) {
                        callBack("Error occured");
                    }
                    return callBack(null, "Success");
                })

        } catch (erorr) {
            console.log(" Error while resetting password " + error)
        }
    }
}
