/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50508
 Source Host           : localhost:3306
 Source Schema         : viitorcloud

 Target Server Type    : MySQL
 Target Server Version : 50508
 File Encoding         : 65001

 Date: 10/07/2021 19:54:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for registration
-- ----------------------------
DROP TABLE IF EXISTS `registration`;
CREATE TABLE `registration`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lastName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of registration
-- ----------------------------
INSERT INTO `registration` VALUES (62, 'janvi', 'solanki', 'janvi@gmail.com', '$2b$10$vcVJgw1NKtdOsz.uCw5D3u6a5qvA9SjyPucY9qacZaQfQOMW5AOhu', 454544454);

SET FOREIGN_KEY_CHECKS = 1;
